# xcolors

Xresources, fbcolors,console-colors e scripts relacionados

## Notas

Esta é uma coleção de temas e scripts para colorir terminais. Inclui *obras originais*, coleções e portas do [linuxbbq](http://www.linuxbbq.org/), [CrunchBang](http://www.crunchbanglinux.org/), [BunsenLabs](https://www.bunsenlabs.org/), [Arch Linux](https://www.archlinux.org/) e [Void Linux](https://www.voidlinux.eu/). Eles são projetados para serem implementados em qualquer coisa de **xterm**, **aterm**, **urxvt**, **stterm**, **fb-term**, e outros consoles do GNU/Linux.

## Créditos

Esta coleção têm como ponto de partida o repositório [Xcolors](https://github.com/linuxbbq/xcolors) criado pela equipe do Linuxbbq + várias implementações feitas por colaboradores das comunidades acima citadas.
